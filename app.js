const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const request = require('request');
const secretKey = '6Lfa2qAbAAAAAGPvrK7AGv-ufoKuPn4Dv-hVGo-h';
const port = 3000

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/static/login.html')
})


app.post('/login', (req, res) => {

    if(!req.body.captcha){
        console.log("err");
        return res.json({"success":false, "msg":"Capctha is not checked"});
       
    }

    const verifyUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}`;

    request(verifyUrl ,(err, response, body)=> {

        if(err){ console.log(err); }

        body = JSON.parse(body);

        if(!body.success && body.success === undefined){
            return res.json({"success":false, "msg":"Captcha verification failed"});
        }
        else if(body.score < 0.5){
            return res.json({"success":false, "msg":"BOT BOT BOTTTTTT", "score": body.score});
        }
        
        return res.json({"success":true, "msg":"Captcha verification passed", "score": body.score});

    })
});

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`)
})

